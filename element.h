#ifndef ELEMENT_H
#define ELEMENT_H

#include "defs.h"

class Element {
  QPointF m_position;
  QPointF m_velocity;
  
  ITEM* m_item;
public:
  Element(SCENE* scene, const QPoint& pos);
  ~Element();
  
  QPointF position() const;
  void setPosition(const QPointF& pos);
  
  QPointF velocity() const;
  void setVelocity(const QPointF& vel);
  
  QPoint size() const;
};

#endif // ELEMENT_H

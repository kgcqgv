#include "mainwindow.h"
#include "element.h"
#include <math.h>
#include <KDebug>
#include <QMouseEvent>
#include <QResizeEvent>

MainWindow::MainWindow(QWidget* parent) 
: CANVAS(parent) {
#ifndef USE_KGC
  m_scene = new QGraphicsScene(this);
  setScene(m_scene);
  
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  
  setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
  setOptimizationFlags( 
    QGraphicsView::DontClipPainter |
    QGraphicsView::DontSavePainterState);
#endif
  
  connect(&m_timer, SIGNAL(timeout()), this, SLOT(tick()));
  
  m_time.start();
  m_last_time = 0;
  m_timer.start(20);
  
  m_size = QSize(600, 600);
}

void MainWindow::resizeEvent(QResizeEvent*) {
#ifndef USE_KGC
  m_scene->setSceneRect(rect());
#endif
}

void MainWindow::addElement(const QPoint& p) {
  SCENE* scene = 
#ifdef USE_KGC
    this;
#else
    m_scene;
#endif
  Element* element = new Element(scene, p);
  m_elements.push_back(element);
  
  kDebug() << m_elements.size() << endl;
}

void MainWindow::tick() {
  int elapsed = m_time.elapsed();
  int delta = elapsed - m_last_time;
  m_last_time = elapsed;

  foreach (Element* item, m_elements) {
    QPointF new_pos = item->position() + item->velocity() * delta;
    QPointF new_vel = item->velocity();
    
    if (new_pos.x() <= item->size().x() / 2) {
      new_pos.setX(item->size().x() - new_pos.x());
      new_vel.setX(fabs(new_vel.x()));
    }
    if (new_pos.x() >= m_size.width() - item->size().x() / 2) {
      new_pos.setX(2 * m_size.width() - item->size().x() - new_pos.x());
      new_vel.setX(-fabs(new_vel.x()));
    }
    if (new_pos.y() <= item->size().y() / 2) {
      new_pos.setY(item->size().y() - new_pos.y());
      new_vel.setY(fabs(new_vel.y()));
    }
    if (new_pos.y() >= m_size.height() - item->size().y() / 2) {
      new_pos.setY(2 * m_size.height() - item->size().y() - new_pos.y());
      new_vel.setY(-fabs(new_vel.y()));
    }
      
    item->setPosition(new_pos);
    item->setVelocity(new_vel);
  }
}

void MainWindow::mousePressEvent(QMouseEvent* e) {
  addElement(e->pos());
}


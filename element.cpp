#include "element.h"
#include <KDebug>

Element::Element(SCENE* scene, const QPoint& point)
: m_position(point - size() / 2.0)
{
#ifdef USE_KGC
  m_item = new KGameCanvasRectangle(Qt::red, QSize(size().x(), size().y()), scene);
  m_item->moveTo(point - size() / 2);
  m_item->show();
#else
  m_item = scene->addRect(QRect(QPoint(0, 0), QSize(size().x(), size().y())), Qt::NoPen, Qt::red);
  m_item->setPos(point - size() / 2);
#endif

  m_velocity.setX(((double)rand() / RAND_MAX) * 0.1);
  m_velocity.setY(((double)rand() / RAND_MAX) * 0.1);
}

Element::~Element() { delete m_item; }

QPointF Element::position() const { return m_position; }

QPointF Element::velocity() const { return m_velocity; }

void Element::setPosition(const QPointF& pos) {
  m_position = pos;
#ifdef USE_KGC
  m_item->moveTo(m_position.toPoint() - size() / 2);
#else
  m_item->setPos(m_position - size() / 2);
#endif
}

void Element::setVelocity(const QPointF& vel) {
  m_velocity = vel;
}

QPoint Element::size() const {
  return QPoint(30, 30);
}


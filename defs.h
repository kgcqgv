#ifndef DEFS_H
#define DEFS_H

#ifdef USE_KGC
#include <KGameCanvas>
#define CANVAS KGameCanvasWidget
#define SCENE CANVAS
#define ITEM KGameCanvasRectangle
#else // USE_KGC
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#define CANVAS QGraphicsView
#define SCENE QGraphicsScene
#define ITEM QGraphicsRectItem
#endif // USE_KGC

#endif // DEFS_H


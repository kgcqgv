#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "defs.h"
#include <QTime>
#include <QTimer>

class Element;

class MainWindow : public CANVAS {
Q_OBJECT
#ifndef USE_KGC
  QGraphicsScene* m_scene;
#endif

  QList<Element*> m_elements;
  QTime m_time;
  QTimer m_timer;
  int m_last_time;
  QSize m_size;
protected:
  void mousePressEvent(QMouseEvent*);
  void resizeEvent(QResizeEvent*);
public:
  MainWindow(QWidget* parent);
  
  void addElement(const QPoint& p);
public Q_SLOTS:
  void tick();
};

#endif // MAINWINDOW_H
